function randomizeHTMLlist(id) {
    // Mélange la liste <ul> (ou <ol>) dont l'id est donné en argument
    var ul = document.getElementById(id);
    const temp = document.createElement("li")
    ul.appendChild(temp)
    for (let i = ul.children.length - 1; i > 0; i--) {
        // Inspiré de Laurens Holst https://stackoverflow.com/a/12646864
        const j = Math.floor(Math.random() * (i + 1));
        // Swap childen i and j
        temp.after(ul.children[j])
        ul.children[j].after(ul.children[i])
        ul.children[i].after(temp)
        ul.removeChild(temp)
    }
}

async function getrandomitem(vitrine, kind, gitlab, root, number) {
    // Récupération d'un objet au hasard
    response = await fetch(root + "/curation/" + kind + "/" + number + ".json");
    const curationdata = await response.json()

    // Récupération des infos depuis gitlab
    // Il faut être authentifié pour avoir accès aux données des utilisateur·ice·e.
    // On triche en faisant une recherche, ce qui est permis aux anonymes.
    response = await fetch(gitlab + "/api/v4/" + kind + (
        (kind == "users") ? ("?username=" + curationdata["path"]) : ("/" + curationdata["id"])
    ));
    var gitlabdata = await response.json();
    if (kind == "users") {
        gitlabdata = gitlabdata[0];
    }

    vitrine.querySelector("img.vitrine-avatar").src = gitlabdata["avatar_url"] ? gitlabdata["avatar_url"] : "https://forge.apps.education.fr/uploads/-/system/appearance/header_logo/1/logo_forge.png";
    vitrine.querySelector("a.vitrine-name").href = gitlabdata["web_url"];
    vitrine.querySelector("a.vitrine-avatar").href = gitlabdata["web_url"];
    vitrine.querySelector("a.vitrine-name").innerHTML = gitlabdata["name"];
    vitrine.querySelector("div.vitrine-description").innerHTML = curationdata["description"];
    vitrine.removeAttribute("hidden");
}

// Columns

async function get_vitrine_sizes(root) {
  // TODO : Ceci suppose que tout se passe bien. Il faut gérer les erreurs
  var vitrinesize = {};
  for (const kind of ["groups", "users", "projects"]) {
    var response = await fetch(root + "/curation/" + kind + ".json");
    var size = await response.json();
    vitrinesize[kind] = size;
  };
  return vitrinesize;
}

async function init_vitrine_columns(root, gitlab) {
  let vitrinesize = await get_vitrine_sizes(root);
  document.querySelectorAll(".vitrine .vitrine-item").forEach(vitrine =>
    {
      vitrine.dataset.size = vitrinesize[vitrine.dataset.kind];
      getrandomitem(
        vitrine,
        vitrine.dataset.kind,
        gitlab,
        root,
        Math.floor(Math.random() * vitrine.dataset.size),
      );
    }
  );
}

async function resetcolumn(column, root, gitlab) {
  // Number of item per column
  number = 3;

  // Delete old vitrines
  column.querySelectorAll(".vitrine-item").forEach(item => {
    item.remove();
  });

  // Are we dealing with users, groups, projects?
  const kind = column.dataset.kind;

  // Récupération du nombre d'objets
  var response = await fetch(root + "/curation/" + kind + ".json");
  const size = await response.json();
  // Choisit number éléments parmi tous
  numbers = (Array.from(Array(size).keys())).sort(() => 0.5 - Math.random()).slice(0, number)

  // Add new vitrines
  template = document.getElementById("vitrine-template");
  for (let i = 0; i < number; i++) {
    // Wait one second
    await new Promise(r => setTimeout(r, 500));
    // Clone template
    clone = template.cloneNode(true);
    clone.removeAttribute("id");
    column.appendChild(clone);
    // Fetch real data
    await getrandomitem(clone, kind, gitlab, root, numbers[i]);
    // Show item
    clone.removeAttribute("hidden");
    // TODO: Animer? Commencer avec l'opacity à 0, puis animer l'opacity à 1
  }
};
