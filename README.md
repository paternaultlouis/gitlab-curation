# Gitlab Curation

Affiche un projet/page/groupe de la forge

## Avertissement

- C'est un brouillon, c'est codé avec les pieds, c'est moche. Si on trouve ça bien, on le refait proprement.
- Ce dépôt a vocation à rejoindre le [groupe docs](https://forge.apps.education.fr/docs), à fusionner avec [console](https://forge.apps.education.fr/docs/console), ou à mourir.

## Exemple

[Exemple](https://paternaultlouis.forge.apps.education.fr/gitlab-curation/)

N'hésitez pas à :

- cliquer sur « 🔁 Un autre… » pour avoir un nouvel élément (choisis pour le moment parmi une banque de deux ou trois projets, groupes, ou personnes, donc vous aurez prabablement plusieurs fois la même chose).
- cliquer sur « Tous » pour avoir la liste de tous les projets/groupes/personnes mis en avant

## Comment ça marche ?

On maintient à jour une fiche par personne dans [ce répertoire](https://forge.apps.education.fr/paternaultlouis/gitlab-curation/-/tree/main/curation). Par exemple, la fiche du groupe @coopmaths est :

~~~yaml
id: 805
path: coopmaths
name: Coopmaths
description: Professeurs de mathématiques qui développent le site [https://coopmaths.fr](https://coopmaths.fr) et ses différents outils.
~~~

Le reste des infos sont piochées dans [l'API de la Forge](https://docs.gitlab.com/ee/api/rest/).

L'idée étant qu'on facilite, autorise et encourage n'importe qui à contribuer pour ajouter des groupes, projets, personnes intéressants.
