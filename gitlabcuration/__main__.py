# Copyright 2024 Louis Paternault
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.

"""Take a curated list of projects/groups/users, and generate json and HTML files."""

import argparse
import json
import pathlib
import random
import tomllib

import jinja2
import markdown
import yaml


def selection2html(url, kind, selection, dest):
    """Convert project/group/user selection to HTML pages."""
    # Génération du fichier de destination
    (dest.parent).mkdir(exist_ok=True)
    with open(dest, encoding="utf8", mode="w") as destfile:
        template = jinja2.Environment(
            loader=jinja2.PackageLoader(__package__, "templates")
        ).get_template(f"{kind}.html")
        destfile.write(
            template.render(
                {
                    "SELECTION": random.sample(selection, k=len(selection)),
                    "URL": url,
                }
            )
        )


def selection2json(selection, dest):
    """Convert project/group/user selection to json files."""
    # Création de projects.json qui contient le nombre de projets
    dest.mkdir(exist_ok=True, parents=True)
    with open(dest.with_suffix(".json"), encoding="utf8", mode="w") as file:
        file.write(json.dumps(len(selection)))
    # Création des fichiers projects/01.json qui contient l'id du projet, et sa description
    for i, item in enumerate(random.sample(selection, k=len(selection))):
        with open(dest / f"{i}.json", encoding="utf8", mode="w") as file:
            file.write(json.dumps(item))


def get_selection(source: pathlib.Path):
    """Read source files"""
    selection = []
    for filename in source.glob("*.yaml"):
        with open(filename, encoding="utf8") as sourcefile:
            data = yaml.safe_load(sourcefile)
            data["description"] = markdown.markdown(data["description"])
            selection.append(data)
    return selection


def main():
    """Main function"""

    # Argument parser
    parser = argparse.ArgumentParser(
        prog="gitlabcuration",
        # pylint: disable=line-too-long
        description="Convert a curated list of projets/users/groups (as .yaml files) into json and html files.",
    )
    parser.add_argument("filename", help="Configuration file.")
    filename = pathlib.Path(parser.parse_args().filename)

    # Open configuration file
    with open(filename, mode="rb") as configfile:
        config = tomllib.load(configfile)

    source = filename.parent / config["SOURCE"]
    dest = filename.parent / config["DEST"]

    # Perform conversion to json and HTML
    for kind in ("groups", "users", "projects"):
        selection = get_selection(source / kind)
        # Génération des listes
        selection2html(config["URL"], kind, selection, dest / f"{kind}.html")
        # Génération des fichiers json
        selection2json(selection, dest / "curation" / kind)


if __name__ == "__main__":
    main()
